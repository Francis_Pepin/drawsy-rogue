const {
    app,
    BrowserWindow
} = require('electron')
const url = require("url");
const path = require("path");

let appWindow

function initWindow() {
    appWindow = new BrowserWindow({
        title: "Drawsy",
        height: 1080,
        width: 1920,
        webPreferences: {
            nodeIntegration: true,
        },
        icon: path.join(__dirname, '/dist/client/assets/logo/feather_logo.ico')
    })

    // Electron Build Path
    appWindow.loadURL(
        url.format({
            pathname: path.join(__dirname, `/dist/client/index.html`),
            protocol: "file:",
            slashes: true
        })
    );
    appWindow.setMenuBarVisibility(false)
    appWindow.maximize();

    // Initialize the DevTools.
    // appWindow.webContents.openDevTools()

    appWindow.on('closed', function () {
        appWindow = null
    })
}

app.on('ready', initWindow)

// Close when all windows are closed.
app.on('window-all-closed', function () {

    // On macOS specific close process
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', function () {
    if (win === null) {
        initWindow()
    }
})
