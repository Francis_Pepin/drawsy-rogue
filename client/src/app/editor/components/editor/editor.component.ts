import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ThemeService } from '@app/app/services/theme.service';
import { DrawingService } from '@app/drawing/services/drawing.service';
import { Vec2 } from '@app/shared/classes/vec2';
import { MouseButton } from '@app/shared/enums/mouse-button.enum';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
})
export class EditorComponent implements AfterViewInit, OnInit, OnDestroy {
    @ViewChild('drawingContainer') drawingContainer: ElementRef<HTMLDivElement>;
    @ViewChild('background') background: ElementRef<HTMLDivElement>;

    private forceDetectChangesSubscription: Subscription;
    private drawingLoadedSubscription: Subscription;

    constructor(private changeDetectorRef: ChangeDetectorRef, private drawingService: DrawingService, private themeService: ThemeService) {}

    ngOnInit(): void {
        this.forceDetectChangesSubscription = this.drawingService.forceDetectChanges$.subscribe(() => {
            this.changeDetectorRef.detectChanges();
        });
        this.drawingLoadedSubscription = this.drawingService.drawingLoaded$.subscribe(() => {
            this.drawingService.resetCanvasView();
        });

        // @HostListener does not support setting the passive option needed to call event.preventDefault() on a passive event
        document.addEventListener(
            'wheel',
            (event: WheelEvent) => {
                this.onScroll(event);
            },
            { passive: false }
        );
    }

    ngAfterViewInit(): void {
        this.drawingService.drawingContainer = this.drawingContainer;
        this.themeService.background = this.background;
    }

    ngOnDestroy(): void {
        this.forceDetectChangesSubscription.unsubscribe();
        this.drawingLoadedSubscription.unsubscribe();
    }

    onScroll(event: WheelEvent): void {
        if (event.ctrlKey) {
            event.preventDefault();
            this.drawingService.zoom(event.deltaY < 0);
        }
    }

    @HostListener('document:mousemove', ['$event'])
    onMouseMove(event: MouseEvent): void {
        if (this.isRightMouseButtonHeld) {
            this.drawingService.applyClampedCanvasTranslation(event);
        }
    }

    @HostListener('document:mousedown', ['$event'])
    onMouseDown(event: MouseEvent): void {
        if (event.button === MouseButton.Right) {
            this.drawingService.isRightMouseButtonHeld = true;
        }
    }

    @HostListener('document:mouseup', ['$event'])
    onMouseUp(event: MouseEvent): void {
        if (event.button === MouseButton.Right) {
            this.drawingService.isRightMouseButtonHeld = false;
        }
    }

    get isRightMouseButtonHeld(): boolean {
        return this.drawingService.isRightMouseButtonHeld;
    }

    get translation(): Vec2 {
        return this.drawingService.translation;
    }

    set translation(translation: Vec2) {
        this.drawingService.translation = translation;
    }
}
