export default {
    minimumZoomPercent: 10,
    maximumZoomPercent: 500,
    defaultZoomPercent: 100,
    zoomMultiplier: 1.05,
};
