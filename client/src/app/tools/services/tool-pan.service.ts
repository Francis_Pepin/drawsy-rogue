import { Injectable, RendererFactory2 } from '@angular/core';
import { ColorService } from '@app/drawing/services/color.service';
import { DrawingService } from '@app/drawing/services/drawing.service';
import { HistoryService } from '@app/drawing/services/history.service';
import { MouseButton } from '@app/shared/enums/mouse-button.enum';
import { TouchService } from '@app/shared/services/touch.service';
import ToolInfo from '@app/tools/constants/tool-info';
import { Tool } from '@app/tools/services/tool';

@Injectable({
    providedIn: 'root',
})
export class ToolPanService extends Tool {
    private isHoldingLeftClick: boolean;

    constructor(
        rendererFactory: RendererFactory2,
        drawingService: DrawingService,
        colorService: ColorService,
        historyService: HistoryService
    ) {
        super(rendererFactory, drawingService, colorService, historyService, ToolInfo.Pan);
        this.isHoldingLeftClick = false;
    }

    onMouseDown(event: MouseEvent): void {
        if (event.button === MouseButton.Left) {
            this.isHoldingLeftClick = true;
        }
    }

    onMouseUp(event: MouseEvent): void {
        if (event.button === MouseButton.Left) {
            this.isHoldingLeftClick = false;
        }
    }

    onMouseMove(event: MouseEvent): void {
        if (this.isHoldingLeftClick) {
            this.drawingService.applyClampedCanvasTranslation(event);
        }
    }

    onPinchTouchMove(event: TouchEvent): void {
        if (event.touches.length <= 1) {
            this.onMouseMove(TouchService.getMouseEventFromTouchEvent(event));
            return;
        }

        const scrollEvent = TouchService.getScrollEventFromTouchEvent(event);
        this.drawingService.zoom(true, Math.abs(scrollEvent.deltaY));
    }

    onPinchTouchStart(event: TouchEvent): void {
        const mouseEventMock = TouchService.getMouseEventFromTouchEvent(event);
        this.onMouseDown(mouseEventMock);
    }

    onPinchTouchEnd(event: TouchEvent): void {
        const mouseEventMock = TouchService.getMouseEventFromTouchEvent(event);
        this.onMouseUp(mouseEventMock);
    }
}
