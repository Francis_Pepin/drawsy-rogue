export enum BackgroundPattern {
    None,
    Paper,
    Boxes,
    Polka,
    Checkered,
}
