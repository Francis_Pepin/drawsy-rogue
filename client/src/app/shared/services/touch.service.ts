import { Vec2 } from '@app/shared/classes/vec2';
import { MouseButton } from '@app/shared/enums/mouse-button.enum';

export class TouchService {
    static isMobileDevice = 'ontouchstart' in document.documentElement;
    private static previousPositions: Vec2[] = [];

    static getMouseEventFromTouchEvent(event: TouchEvent): MouseEvent {
        let movementX = 0;
        let movementY = 0;

        if (event.type === 'touchmove') {
            movementX = event.changedTouches[0].clientX - this.previousPositions[0].x;
            movementY = event.changedTouches[0].clientY - this.previousPositions[0].y;
        }

        const mouseMockEvent = {
            button: MouseButton.Left,
            clientX: event.changedTouches[0].clientX,
            clientY: event.changedTouches[0].clientY,
            movementX,
            movementY,
        } as MouseEvent;

        TouchService.previousPositions[0] = { x: mouseMockEvent.clientX, y: mouseMockEvent.clientY };
        if (event.changedTouches.length > 1) {
            TouchService.previousPositions[1] = { x: event.changedTouches[1].clientX, y: event.changedTouches[1].clientY };
        }

        if (event.type === 'touchend') {
            TouchService.previousPositions = [];
        }

        return mouseMockEvent;
    }

    static getScrollEventFromTouchEvent(event: TouchEvent): WheelEvent {
        const firstTouch = { x: event.changedTouches[0].clientX, y: event.changedTouches[0].clientY };
        const secondTouch = { x: event.changedTouches[1].clientX, y: event.changedTouches[1].clientY };

        const previousDistance = Vec2.distance(
            TouchService.previousPositions[0] ?? firstTouch,
            TouchService.previousPositions[1] ?? secondTouch
        );
        const currentDistance = Vec2.distance(firstTouch, secondTouch);

        if (event.type === 'touchend') {
            TouchService.previousPositions = [];
        } else {
            TouchService.previousPositions[0] = firstTouch;
            TouchService.previousPositions[1] = secondTouch;
        }

        return {
            deltaY: currentDistance / previousDistance,
        } as WheelEvent;
    }
}
